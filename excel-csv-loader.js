'use strict';

module.exports = function(source) {
  this.cacheable();
  let stringData = source.split(/\r?\n/);
  let data = new Array(stringData.length - 1);
  for (let i = 0; i < stringData.length - 1; ++i) {
    let parts = stringData[i].split(';');
    data[i] = new Array(parts.length - 1);
    for (let j = 0; j < parts.length - 1; ++j)
    {
      if (/^[+-]?\d+$/.test(parts[j]))
        data[i][j] = parseInt(parts[j]);
      else if (/^[-+]?\d+\,\d+$/.test(parts[j]))
        data[i][j] = parseFloat(parts[j]);
      else
        data[i][j] = parts[j];
    }
  }
  return 'module.exports = ' + JSON.stringify(data) + ';';
};
